package navi.kids.com.kidsnavi.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import navi.kids.com.kidsnavi.R;
import navi.kids.com.kidsnavi.utils.view.VerificationCodeView;

public class VerificationActivity extends AppCompatActivity {
    @BindView(R.id.vcv_code)
    VerificationCodeView vcvCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);

    }


    @OnClick({R.id.btn_resend_code, R.id.btn_verify, R.id.btn_not_recived})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_resend_code:
                break;
            case R.id.btn_verify:
                vcvCode.getTextString();
                break;
            case R.id.btn_not_recived:
                break;
        }
    }
}

package navi.kids.com.kidsnavi.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import navi.kids.com.kidsnavi.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
